import React from 'react';
import ReactDOM from 'react-dom';
import PageFooter from './footer.js';
import "./content.css";

export default class Content extends React.Component{
    render(){
        const menuOne = 'app/gallery/menu2.jpg';
        const menuTwo = 'app/gallery/menu1.jpg';
        const imageThree = 'app/gallery/about3.jpg';
        const imageFour = 'app/gallery/about4.jpg';
        const imagePark = 'app/gallery/park.jpg';
        const imageWoods = 'app/gallery/woods.jpg';
        const imageFood = 'app/gallery/food.jpg';
        const imageOrder = 'app/gallery/order.jpg';
        const imageTerras = 'app/gallery/terras.jpeg';
        const dots = 'app/images/dots.png';
        const menuText = 'Our love for excessively sugary treats is no joke, but we may have just met our match with these eight too delish to quit desserts. We\'ve never seen anything quite like them, which is why we thought we\'d share them with you. Word to thewise, don\'t try to make them all at once or you will all into the worst food coma in the history of your existence.';
        const reservationText = 'The Westby House has a long history, begining with the original 2 story 8-room house built with the house and pharmacy. In 1892, the pharmacy was sold to O.E Unseth and the house to Martin H. Bekkedal.Mr. Bekkedal, who emigrated from Norway at age 21, was a keen entrepreneur. He married Amanda Anderson in 1890.';
        const contactText = 'Our love for excessively sugary treats is no joke, but we may have just met our match to quit desserts. We\'ve never seen anything quite like them, which is why we thought we\'d share them with you. Word to the wise, don\'t try to make them all at once or you will fall into the worst food coma in the history of your existence.';

        return(
            <div>
                <div className="menu">
                    <div className="imageContainer">
                        <img className="aboutOne" alt="Desserts" src={menuOne} />
                        <img className="aboutTwo" alt="Cake" src={menuTwo} />
                    </div>
                    <article className="textContainer lunchText">
                        <header>
                            <h1 className="menuHeader">menu</h1>
                        </header>
                        <section>
                            <p className="topicMenu">DELICIOUS AND BEAUTIFUL</p>
                            <p className="dessert">{menuText}
                            </p>
                            <a href="#"><img className = 'imageDotMenu' alt="Read more" src={dots} /></a>
                        </section>
                    </article>
                </div>
                <img className="terras" alt="" src={imageTerras} />
                <h2 className="romanticHeader">"The most romantic and perfect place"</h2>
                <p className="author">JOHN DOE</p>
                <div className="imageContainer">
                    <img className="aboutThree" alt="Dish one" src={imageThree} />
                    <img className="aboutFour" alt="Dish two" src={imageFour} />
                </div>
                <article>
                    <header className="reservationHeader">
                        <h1>reservation</h1>
                    </header>
                    <section>
                        <p className="topic">BOOK YOUR TABLE AT OUR RESTAURANT NOW!</p>
                        <p className="text">{reservationText}
                        </p>
                        <a href="#"><img className = 'imageDot' alt="Read more" src={dots} /></a>
                    </section>
                </article>
                <article className="serviceContainer">
                    <header>
                        <h1 className="serviceHeader">our services</h1>
                    </header>
                    <section>
                        <p className="topicService">ADVANTAGES OF OUR RESTAURANT</p>
                        <div className="serviceAdvatagesBorders">Pickup or Delivery</div>
                        <div className="serviceAdvatagesBorders">Serving With Love</div>
                        <div className="serviceAdvantages">Daily Lunch Specials</div>
                        <div className="orderContainer">
                            <img className="order" alt="Dish" src={imageOrder} />
                            <div className="orderButtonContainer">
                                <a className="orderButton" href="">ORDER NOW</a>
                            </div>
                        </div>
                    </section>
                </article>
                <article className="blogContainer">
                    <header>
                        <h1>blog</h1>
                    </header>
                    <section>
                        <p className="topicService">NEWS, RECIPES AND MUCH MORE</p>
                        <div className="blogTopicContainer">
                            <img className="park" alt="park" src={imagePark} />
                            <div className="blogTopicBackground">
                                <div className="blogTopicText">
                                    <p className="blogTopicDate">10/03</p>
                                    <p className="blogTopicName">GRAND OPENING </p>
                                    <p className="readMore">•••</p>
                                </div>
                            </div>
                        </div>
                        <div className="blogTopicContainer">
                            <img className="woods" alt="House in woods" src={imageWoods} />
                            <div className="blogTopicBackground">
                                <div className="blogTopicText">
                                    <p className="blogTopicDate">07/03</p>
                                    <p className="blogTopicName">MINI CHILLI PRETZELS </p>
                                    <p className="readMore">•••</p>
                                </div>
                            </div>
                        </div>
                        <div className="blogTopicContainer">
                            <img className="food" alt="Dish of food" src={imageFood} />
                            <div className="blogTopicBackground">
                                <div className="blogTopicText">
                                    <p className="blogTopicDate ">05/06</p>
                                    <p className="blogTopicName">SUMMER SPECIALS </p>
                                    <p className="readMore">•••</p>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
                <div className="imageContainer">
                    <img className="aboutOne" alt="Desserts" src={menuTwo} />
                    <img className="aboutTwo" alt="Cake" src={imageFour} />
                </div>
                <div className="contactContainer">
                    <article className="textContainer contactText">
                        <header>
                            <h1 className="contactHeader">contact</h1>
                        </header>
                        <section>
                            <p className="topicContact">WE WELCOME YOU IN OUR RESTAURANT</p>
                            <p className="contacts">{contactText}
                            </p>
                            <a href="#"><img className = 'imageDotMenu' alt="Read more" src={dots} /></a>
                        </section>
                    </article>
                </div>
                <PageFooter />
            </div>
        );
    }
}