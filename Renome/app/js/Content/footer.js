import React from 'react';
import ReactDOM from 'react-dom';
import './footer.css';

export default class PageFooter extends React.Component{
    render(){
        const facebookIcon = 'app/images/face.svg';
        const twiterIcon = 'app/images/twit.svg';
        const googleIcon = 'app/images/google.svg';
        const logoImage = 'app/images/logo.svg';
        
        return(
            <div className="footer">
                <p className="footerTopic">FOLLOW US:</p>
                <div className="footerLogos">
                    <a href="#facebook"><img className = 'image imageBorder' alt="menu" src={facebookIcon} /></a>
                    <a href="#twitter"><img className = 'image imageBorder' alt="menu" src={twiterIcon} /></a>
                    <a href="#google+"><img className = 'image imageBorder' alt="menu" src={googleIcon} /></a>
                </div>
                <br />
                <img className = 'footerLogo ' alt="Logo" src={logoImage} />
                <p className="footerCopyright">COPYRIGHT 2015 RENOME BY ESTETIQ</p>
            </div>
        );
    }
}