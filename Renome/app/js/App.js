import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import About from './About/index.js';
import Navigation from './Navigation/index.js';
import Content from './Content/content.js';
import Page from './Carousel/index.js';
import Slider from './CarouselTwo/index.js';

export default class App extends Component {
    constructor(){
        super();
        this.state ={
            sliderOneImageList: [
                {id: "1", src: 'app/gallery/toast.jpg'},
                {id: "2", src: 'app/gallery/bacon.jpg'},
                {id: "3", src: 'app/gallery/eggs.jpg'},
            ],
            sliderTwoImageList: [
                {id: "1", src: 'app/gallery/baget.jpg', price: "3,76", description: 'Bla lba'},
                {id: "2", src: 'app/gallery/sandwitch.jpg', price: "5,76", description: 'Blu blo'},
                {id: "3", src: 'app/gallery/cheburek.jpg', price: "9,45", description: 'Bli blong'},
            ]
        } 
    }
    render(){
        const aboutImageList = [
            {id:1, class: 'aboutOne', alt:'Restaurant one', src:'app/gallery/about1.jpg'},
            {id:2, class: 'aboutTwo', alt:'Restaurant two',src:'app/gallery/about2.jpg'},
        ];
        return(
            <div>
                <Navigation />
                <Page sliderOneImageList={this.state.sliderOneImageList} />
                <About aboutImageList={aboutImageList} />
                <Slider sliderTwoImageList={this.state.sliderTwoImageList} />
                <Content/>
            </div>
        );
    }
}