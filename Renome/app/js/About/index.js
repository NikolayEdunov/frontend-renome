import React from 'react';
import ReactDOM from 'react-dom';
import "./about.css"

export default class About extends React.Component {
    render(){
        let aboutImg;
        const dots = 'app/images/dots.png'
        const aboutText = 'The Westby House has a long history, begining with the original 2 story 8-room house built by Westby’s first pharmacist, Erling Ramsland, who immigrated from Stavanger, Norway, and his wife Regina in 1886. In 1891, Mr. Ramsland died from heart complications t a very young age, leaving his wife and two small children struggling with the house and pharmacy. In 1892, the pharmacy was sold to O.E Unseth and the house to Martin H. Bekkedal. Mr. Bekkedal, who emigrated from Norway at age 21, was a keen entrepreneur. He married Amanda Anderson in 1890.';
        if(this.props.aboutImageList){
            aboutImg = this.props.aboutImageList.map(image=>{
                return(
                    <img key={image.id} className={image.class} alt={image.alt} src={image.src} />
                );
            });
        }
        return (
            <div>
            <div className="aboutContainer">
                <div className="imageContainer">
                    {aboutImg}
                </div>
                <article className="textContainer">
                    <header>
                        <h1>about</h1>
                    </header>
                    <section>
                        <p className="topic">WE CREATE DELICIOUS MEMORIES</p>
                    <p className="text">
                        {aboutText}
                    </p>
                    <a href="#"><img className = 'imageDot' alt="Read more" src={dots}/></a>
                    </section>
               </article>
             </div>
        </div>
        );
    }
}
