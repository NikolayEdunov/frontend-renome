import React from 'react';
import ReactDOM from 'react-dom';

export default class Drop extends React.Component {
    render() {
        return <div className={this.props.className}
                onClick={this.props.toggleClassName}
                >
                    { this.props.children }
                </div>
    }
}