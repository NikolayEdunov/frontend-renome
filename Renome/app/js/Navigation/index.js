
import React from 'react';
import ReactDOM from 'react-dom';
import Drop from './drop.js';
import "./navigation.css";

export default class Navigation extends React.Component {
    constructor(props) {    
        super(props)
        this.state = {cart: false}
        this.state = {main: false}
        this.handleClickCart = this.handleClickCart.bind(this)
        this.handleClickMain = this.handleClickMain.bind(this)
    }
    handleClickCart() {
        if(this.state.main){
            this.setState({
                main: !this.state.main
            })
        }
        this.setState({
        cart: !this.state.cart
        })
    }
    handleClickMain() {
        if(this.state.cart){
            this.setState({
                cart: !this.state.cart
            })
        }
        this.setState({
        main: !this.state.main
        })
    }
    render() {
        return(
        <div>
            <nav>
                <ul>
                    <li><img className = 'imageLogo' alt="Logo" src="app/images/logo.svg" /></li>
                    <li className = 'right'><a className="dropButton" onClick={ this.handleClickMain }><img className = 'image imageLineBorder' alt="menu" src="app/images/hamburger.png" /></a></li>   
                    <li className = 'right'><a className="dropButton" onClick={ this.handleClickCart }><img className = 'image' alt="Cart" src="app/images/cart.svg" /></a></li>
                </ul>
            </nav>
            <div className="dropdown">
                <Drop className={ this.state.cart ? "cartDropdownContent show" : "cartDropdownContent" }
                toggleClassName={ this.handleClickCart }>
                <div>
                    <a className="cartDrop" href="#price">$44.50</a>
                    <a className="cartDrop" href="#view">View Cart</a>
                    <a className="cartDrop" href="#Checkout">Checkout</a>
                </div>
                </Drop> 

                <Drop className={ this.state.main ? "dropdownContent show" : "dropdownContent" }
                toggleClassName={ this.handleClickMain }>
                    <div className="searchContainer">
                            <input type="text" className="search" placeholder="Search..." title="Type in a name" />
                            <img className="image" src="app/images/search.png" />
                    </div>
                    <a className="drop" href="#home">HOME</a>
                    <a className="drop" href="#about">ABOUT</a>
                    <a className="drop" href="#menu">MENU</a>
                    <a className="drop" href="#reservation">RESERVATION</a>
                    <a className="drop" href="#blog">BLOG</a>
                    <div className="drop">
                        <a className="feature" href="#features">FEATURES<span className="arrowMore" href="#features-more">❯</span></a>
                    </div>
                    <a className="drop" href="#shop">SHOP</a>
                    <a className="drop" href="#contact">CONTACT</a>
                </Drop> 
            </div>
        </div>
        );
    }
}
