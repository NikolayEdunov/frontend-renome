import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Slides extends React.Component {
    render() {
        return (
            <img className='slideUnion' src={this.props.slider.src} />
        );
    }
}
