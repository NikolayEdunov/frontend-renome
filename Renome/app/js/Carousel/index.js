import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Slides from './slides.js'
import "./carousel.css";

export default class Page extends React.Component {
    constructor() {
        super();
        this.state = {showSlide: 'sliderLine slideOne'}
        this.handleNextClick = this.handleNextClick.bind(this);
        this.handlePrewClick = this.handlePrewClick.bind(this);
    }

    handleNextClick() {
        if(this.state.showSlide == 'sliderLine slideThree'){
            this.setState({
                showSlide: 'sliderLine slideOne',
            });
        }else if(this.state.showSlide == 'sliderLine slideOne'){
            this.setState({
                showSlide: 'sliderLine slideTwo',
            });
        }else{
            this.setState({
                showSlide: 'sliderLine slideThree',
            });
        }
    }
    handlePrewClick() {
        if(this.state.showSlide == 'sliderLine slideThree'){
            this.setState({
                showSlide: 'sliderLine slideTwo',
            });
        }else if(this.state.showSlide == 'sliderLine slideTwo'){
            this.setState({
                showSlide: 'sliderLine slideOne',
            });
        }else{
            this.setState({
                showSlide: 'sliderLine slideThree',
            });
        }
    }
    render() {
        let sliderOneImage;
        if(this.props.sliderOneImageList){
            sliderOneImage=this.props.sliderOneImageList.map(slider=>{
                return(
                    <Slides key ={slider.id} slider={slider} />
                );
            });
        }
        return (
        <div className="photoSlider">
            <ul>
                <li><a className="previous control" onClick={ this.handlePrewClick }><img className='arrow' src='app/images/arrowleft.png'/></a></li>   
                <li><a className="next control" onClick={ this.handleNextClick }><img className='arrow' src='app/images/arrowright.png'/></a></li>
            </ul>
            <div className={this.state.showSlide}>
                {sliderOneImage}
            </div>
        </div>
        );
    }
}
