import React from 'react';
import ReactDOM from 'react-dom';
import Slides from './slides.js';
import "./carousel.css";

export default class Slider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showSlide: 'sliderLine slideLunchOne'};
        this.buttonOne = this.buttonOne.bind(this);
        this.buttonTwo = this.buttonTwo.bind(this);
        this.buttonThree = this.buttonThree.bind(this);
        this.state.sliderButton={
            activeButtonOne:true,
            activeButtonTwo:false,
            activeButtonThree:false
        };
    }
    buttonOne() {
        this.setState({
            showSlide: 'sliderLine slideLunchOne',
            sliderButton:{
                activeButtonOne:true,
                activeButtonTwo:false,
                activeButtonThree:false
            }
        })
    }
    buttonTwo() {
        this.setState({
            showSlide: 'sliderLine slideLunchTwo',
            sliderButton:{
                activeButtonOne:false,
                activeButtonTwo:true,
                activeButtonThree:false
            }
        })
    }
    buttonThree() {
        this.setState({
            showSlide: 'sliderLine slideLunchThree',
            sliderButton:{
                activeButtonOne:false,
                activeButtonTwo:false,
                activeButtonThree:true
            }
        })
    }

    render() {
        let sliderTwoImage;
        if(this.props.sliderTwoImageList){
            sliderTwoImage=this.props.sliderTwoImageList.map(slider=>{
                return(
                    <Slides key ={slider.id} slider={slider} />
                );
            });
        }
        return (
        <div className="lunchContainer">
            <div className="textContainer">
                <h1 className="lunchHeader">lunch</h1>
                <p className="lunchTopic">OF THE DAY</p>
                <div className="photoSlider photoSliderLunch">
                    <div className={this.state.showSlide}>
                        {sliderTwoImage}
                    </div>
                    <div className='dots'>
                        <div className={this.state.sliderButton.activeButtonOne ? 'dotStyle dotActive' : 'dotStyle'} onClick={ this.buttonOne }></div>
                        <div className={this.state.sliderButton.activeButtonTwo ? 'dotStyle dotActive' : 'dotStyle'} onClick={ this.buttonTwo }></div>   
                        <div className={this.state.sliderButton.activeButtonThree ? 'dotStyle dotActive' : 'dotStyle'} onClick={ this.buttonThree }></div>
                    </div>
                </div>
                <img className="day" alt="Monday" src="app/gallery/monday.jpg"/>
            </div>
        </div>

        );
    }
}
