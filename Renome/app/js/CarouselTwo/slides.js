import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Slides extends React.Component {
    render() {
        return (
            <div className='slideUnion'>
                <img className="baget" alt="Bagguett" src={this.props.slider.src} /> 
                <p className="description">{this.props.slider.description}</p>
                <p className="price">{this.props.slider.price}</p>
            </div>
        );
    }
}