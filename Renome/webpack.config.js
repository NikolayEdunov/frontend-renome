module.exports = {
    entry: './app/js/script.js',
    output: {
        path: __dirname +'/dist',
        filename: 'bundle.js'
    },
    module:{
        rules: [
            {
              test: /\.css$/,
              use: [
                { loader: "style-loader" },
                { loader: "css-loader" },
                { loader: "postcss-loader"}
              ]
            },
            {
                test: /\.js$/, 
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options:{
                    presets:['es2015']}
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    }
}